package com.example.todo_list.di.network;

import com.example.todo_list.App;
import com.example.todo_list.config.BuildConfigHelper;
import com.example.todo_list.network.ApiErrorHandler;
import com.example.todo_list.network.NetworkConnectionHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.DateFormat;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module (includes = {TodoNetworkModule.class})
public class NetworkModule {

    @Provides
    @Singleton
    public HttpLoggingInterceptor provideInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        HttpLoggingInterceptor.Level level = BuildConfigHelper.isProduction() ? HttpLoggingInterceptor.Level.BASIC : HttpLoggingInterceptor.Level.BODY;
        interceptor.setLevel(level);

        return interceptor;
    }

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient(HttpLoggingInterceptor interceptor) {

        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    public GsonConverterFactory provideConvertFactory() {
        Gson gson = new GsonBuilder()
                .setDateFormat(DateFormat.FULL, DateFormat.FULL)
                .create();

        return GsonConverterFactory.create(gson);
    }


    @Provides
    @Singleton
    public Retrofit provideRetrofit(GsonConverterFactory converterFactory, OkHttpClient client) {

        return new Retrofit.Builder()
                .baseUrl(BuildConfigHelper.getBaseUrl())
                .addConverterFactory(converterFactory)
                .client(client)
                .build();
    }

}

