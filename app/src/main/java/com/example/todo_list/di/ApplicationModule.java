package com.example.todo_list.di;


import android.content.Context;

import com.example.todo_list.App;
import com.example.todo_list.ui.main.di.TodoComponent;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private App mApplication;

    public ApplicationModule(App application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    public App provideApplication() {
        return mApplication;
    }


    @Provides
    @Singleton
    public Context provideApplicationContext() {
        return mApplication;
    }
}

