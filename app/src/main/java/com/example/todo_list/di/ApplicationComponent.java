package com.example.todo_list.di;

import com.example.todo_list.di.network.NetworkModule;
import com.example.todo_list.ui.main.TodoActivity;
import com.example.todo_list.ui.main.di.TodoComponent;
import com.example.todo_list.ui.main.di.TodoMainModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface ApplicationComponent {
    TodoComponent add(TodoMainModule module);
}
