package com.example.todo_list.di.network;

import com.example.todo_list.network.ApiErrorHandler;
import com.example.todo_list.network.NetworkConnectionHelper;
import com.example.todo_list.network.todo.TodoApiService;
import com.example.todo_list.network.todo.TodoRetrofitProvider;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;


@Module
public class TodoNetworkModule {

    @Provides
    public TodoApiService provideApiService(Retrofit retrofit) {
        return retrofit.create(TodoApiService.class);
    }

    @Provides
    public TodoRetrofitProvider provideNetworkProvider(TodoApiService apiService, NetworkConnectionHelper networkConnectionHelper, ApiErrorHandler errorHandler) {
        return new TodoRetrofitProvider(apiService, networkConnectionHelper, errorHandler);
    }

}
