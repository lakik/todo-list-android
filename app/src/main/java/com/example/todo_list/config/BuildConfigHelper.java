package com.example.todo_list.config;


import com.example.todo_list.BuildConfig;

public class BuildConfigHelper {

    public static boolean isProduction() {
        return BuildConfig.BUILD_TYPE.equals("release");
    }

    public static String getBaseUrl() {
        return BuildConfig.BASE_URL;
    }

}
