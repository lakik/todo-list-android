package com.example.todo_list.utils;

import android.content.Context;
import android.content.res.Resources;


import com.example.todo_list.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils {

    public static String getFormattedHour(int hour, int minutes) {
        SimpleDateFormat dateFormat = getFormattedHourPattern();
        Calendar calendar = Calendar.getInstance(/*Locale.getDefault()*/);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minutes);

        return dateFormat.format(calendar.getTime());
    }

    private static SimpleDateFormat getFormattedHourPattern() {
        return new SimpleDateFormat("hh:mma", Locale.getDefault());
    }

    private static SimpleDateFormat getFormattedDatePattern() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return simpleDateFormat;
    }

    public static String getFormattedHour(long millis) {
        SimpleDateFormat dateFormat = getFormattedHourPattern();
        Calendar calendar = Calendar.getInstance(/*Locale.getDefault()*/);
        calendar.setTimeInMillis(millis);

        return dateFormat.format(calendar.getTime());
    }

    public static String getFormattedDate(long millis) {
        SimpleDateFormat dateFormat = getFormattedDatePattern();
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTimeInMillis(millis);

        return dateFormat.format(calendar.getTime());
    }

    public static String millisToServerDate(long millis) {
        SimpleDateFormat formatter = DateUtils.getServerPattern();
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTimeInMillis(millis);
        return formatter.format(calendar.getTime());
    }


    private static SimpleDateFormat getServerPattern() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return simpleDateFormat;
    }


    public static Date currentDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    public static long currentTime() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTimeInMillis();
    }

    public static String getTimeAgo(Context context, long dateLong) {

        Calendar calendar = Calendar.getInstance(); // java.util.Calendar
        calendar.setTimeInMillis(dateLong);
        Date date = calendar.getTime();

        if (date == null) {
            return null;
        }

        long time = date.getTime();

        Date curDate = currentDate();
        long now = curDate.getTime();
        if (time > now || time <= 0) {
            return null;
        }

        int dim = getTimeDistanceInMinutes(time);

        Resources resources = context.getResources();
        String timeAgo;

        if (dim == 0) {
            timeAgo = resources.getString(R.string.date_util_term_less) + " " + resources.getString(R.string.date_util_term_a) + " " + resources.getString(R.string.date_util_unit_minute);
        } else if (dim == 1) {
            return "1 " + resources.getString(R.string.date_util_unit_minute);
        } else if (dim >= 2 && dim <= 44) {
            timeAgo = dim + " " + resources.getString(R.string.date_util_unit_minutes);
        } else if (dim >= 45 && dim <= 89) {
            timeAgo = resources.getString(R.string.date_util_prefix_about) + " " + resources.getString(R.string.date_util_term_an) + " " + resources.getString(R.string.date_util_unit_hour);
        } else if (dim >= 90 && dim <= 1439) {
            timeAgo = resources.getString(R.string.date_util_prefix_about) + " " + (Math.round(dim / 60)) + " " + resources.getString(R.string.date_util_unit_hours);
        } else if (dim >= 1440 && dim <= 2519) {
            timeAgo = "1 " + resources.getString(R.string.date_util_unit_day);
        } else if (dim >= 2520 && dim <= 43199) {
            timeAgo = (Math.round(dim / 1440)) + " " + resources.getString(R.string.date_util_unit_days);
        } else if (dim >= 43200 && dim <= 86399) {
            timeAgo = resources.getString(R.string.date_util_prefix_about) + " " + resources.getString(R.string.date_util_term_a) + " " + resources.getString(R.string.date_util_unit_month);
        } else if (dim >= 86400 && dim <= 525599) {
            timeAgo = (Math.round(dim / 43200)) + " " + resources.getString(R.string.date_util_unit_months);
        } else if (dim >= 525600 && dim <= 655199) {
            timeAgo = resources.getString(R.string.date_util_prefix_about) + " " + resources.getString(R.string.date_util_term_a) + " " + resources.getString(R.string.date_util_unit_year);
        } else if (dim >= 655200 && dim <= 914399) {
            timeAgo = resources.getString(R.string.date_util_prefix_over) + " " + resources.getString(R.string.date_util_term_a) + " " + resources.getString(R.string.date_util_unit_year);
        } else if (dim >= 914400 && dim <= 1051199) {
            timeAgo = resources.getString(R.string.date_util_prefix_almost) + " 2 " + resources.getString(R.string.date_util_unit_years);
        } else {
            timeAgo = resources.getString(R.string.date_util_prefix_about) + " " + (Math.round(dim / 525600)) + " " + resources.getString(R.string.date_util_unit_years);
        }

        return timeAgo + " " + resources.getString(R.string.date_util_suffix);
    }

    private static int getTimeDistanceInMinutes(long time) {
        long timeDistance = currentDate().getTime() - time;
        return Math.round((Math.abs(timeDistance) / 1000) / 60);
    }

    public static long getRoundedMinutesOfHour(int minutesToAdd) {
        Calendar calendar = Calendar.getInstance();

        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        if (isBetween(minute, 0, 15)) {
            calendar.set(Calendar.MINUTE, 15);
        } else if (isBetween(minute, 16, 30)) {
            calendar.set(Calendar.MINUTE, 30);
        } else if (isBetween(minute, 31, 45)) {
            calendar.set(Calendar.MINUTE, 45);
        } else {
            calendar.set(Calendar.HOUR_OF_DAY, hour + 1);
            calendar.set(Calendar.MINUTE, 0);
        }

        calendar.add(Calendar.MINUTE, minutesToAdd);
        return calendar.getTimeInMillis();

    }

    private static boolean isBetween(int x, int lower, int upper) {
        return lower <= x && x <= upper;
    }

}
