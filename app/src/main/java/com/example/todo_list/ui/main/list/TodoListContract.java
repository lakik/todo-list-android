package com.example.todo_list.ui.main.list;

import com.example.todo_list.model.Todo;

import java.util.ArrayList;

public interface TodoListContract {
    interface View {
        void updateTodoList(ArrayList<Todo> todoList);
        void showEmptyView(boolean show);
    }

    interface Presenter {
        void loadTodoList();
        void updateTodoItem(Todo todo);
        void deleteTodoItem(Todo todo);
    }
}
