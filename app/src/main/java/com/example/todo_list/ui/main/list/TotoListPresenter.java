package com.example.todo_list.ui.main.list;

import android.util.Log;

import com.example.todo_list.data.todo.TodoRepository;
import com.example.todo_list.model.Todo;

import java.util.ArrayList;

import javax.inject.Inject;

public class TotoListPresenter implements TodoListContract.Presenter {

    TodoRepository todoRepository;
    TodoListContract.View mView;

    @Inject
    public TotoListPresenter(TodoRepository todoRepository, TodoListContract.View mView) {
        this.todoRepository = todoRepository;
        this.mView = mView;
    }

    @Override
    public void loadTodoList() {
        todoRepository.loadTodoList(new TodoRepository.OnTodoListListener() {
            @Override
            public void OnLoaded(ArrayList<Todo> todoList) {
                mView.updateTodoList(todoList);
            }

            @Override
            public void OnError(String errorMessage) {
                //Todo show error on fragment
                Log.e("error", errorMessage);
            }
        });
    }

    @Override
    public void updateTodoItem(Todo todo) {
        todoRepository.updateTodo(todo, new TodoRepository.OnUpdateTodoListener() {
            @Override
            public void OnUpdated(Todo todo) {
                loadTodoList();
            }

            @Override
            public void OnError(String errorMessage) {
                //Todo show error on fragment
                Log.e("error", errorMessage);
            }
        });
    }

    @Override
    public void deleteTodoItem(Todo todo) {
        todoRepository.deleteTodo(todo, new TodoRepository.OnDeleteTodoListener() {
            @Override
            public void OnDeleted() {

            }

            @Override
            public void OnError(String errorMessage) {
                loadTodoList();
                Log.e("error", errorMessage);
            }
        });
    }
}
