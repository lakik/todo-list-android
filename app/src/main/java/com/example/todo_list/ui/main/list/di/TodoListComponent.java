package com.example.todo_list.ui.main.list.di;

import com.example.todo_list.ui.main.list.TodoListFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {TodoListModule.class})
public interface TodoListComponent {
    void inject(TodoListFragment fragment);
}
