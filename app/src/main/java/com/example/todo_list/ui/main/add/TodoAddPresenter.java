package com.example.todo_list.ui.main.add;

import android.util.Log;

import com.example.todo_list.data.todo.TodoRepository;
import com.example.todo_list.model.Todo;

import javax.inject.Inject;

public class TodoAddPresenter implements TodoAddContract.Presenter {

    TodoRepository todoRepository;
    TodoAddContract.View mView;

    @Inject
    public TodoAddPresenter(TodoRepository todoRepository, TodoAddContract.View mView) {
        this.todoRepository = todoRepository;
        this.mView = mView;
    }

    @Override
    public void createTodo(Todo todo) {
        todoRepository.createTodo(todo, new TodoRepository.OnCreateTodoListener() {
            @Override
            public void OnCreated(Todo todo) {
                mView.closeFragment();
            }

            @Override
            public void OnError(String errorMessage) {
                mView.showCreateError();
                Log.e("error", errorMessage);
            }
        });
    }

    @Override
    public void updateTodo(Todo todo) {
        todoRepository.updateTodo(todo, new TodoRepository.OnUpdateTodoListener() {
            @Override
            public void OnUpdated(Todo todo) {
                mView.closeFragment();
            }

            @Override
            public void OnError(String errorMessage) {
                //Todo show error on fragment
                Log.e("error", errorMessage);
            }
        });
    }
}
