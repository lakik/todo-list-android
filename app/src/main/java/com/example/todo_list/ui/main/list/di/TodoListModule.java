package com.example.todo_list.ui.main.list.di;

import com.example.todo_list.ui.main.list.TodoListContract;

import dagger.Module;
import dagger.Provides;

@Module
public class TodoListModule {

    private TodoListContract.View mView;

    public TodoListModule(TodoListContract.View view) {
        mView = view;
    }

    @Provides
    public TodoListContract.View provideView() {
        return mView;
    }
}
