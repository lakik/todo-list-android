package com.example.todo_list.ui.main;

import com.example.todo_list.data.todo.TodoRepository;

import javax.inject.Inject;

public class TodoPresenter implements TodoContract.Presenter {

    TodoRepository todoRepository;
    TodoContract.View mView;

    @Inject
    public TodoPresenter(TodoRepository todoRepository, TodoContract.View mView) {
        this.todoRepository = todoRepository;
        this.mView = mView;
    }

}
