package com.example.todo_list.ui.main.di;

import com.example.todo_list.ui.main.TodoContract;

import dagger.Module;
import dagger.Provides;

@Module
public class TodoMainModule {

    private TodoContract.View mView;

    public TodoMainModule(TodoContract.View view) {
        mView = view;
    }

    @Provides
    public TodoContract.View provideView() {
        return mView;
    }
}
