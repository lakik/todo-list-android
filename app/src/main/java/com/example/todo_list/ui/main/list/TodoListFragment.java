package com.example.todo_list.ui.main.list;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.Group;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.todo_list.R;
import com.example.todo_list.model.Todo;
import com.example.todo_list.ui.main.TodoActivity;
import com.example.todo_list.ui.main.list.di.TodoListModule;


import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class TodoListFragment extends Fragment implements TodoListContract.View{

    private TodoListListener mListener;

    @Inject TotoListPresenter mPresenter;

    @BindView(R.id.rv_todo_list) RecyclerView recyclerView;
    @BindView(R.id.swiperefresh) SwipeRefreshLayout swiperefresh;
    @BindView(R.id.gpEmpty) Group gpEmptyList;
    private TodoListAdapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;


    private Unbinder unbinder;

    public TodoListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (getActivity() != null) {
            ((TodoActivity) getActivity()).component.add(new TodoListModule(this)).inject(this);
            mListener = (TodoActivity) getActivity();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_todo_list, container, false);
        unbinder = ButterKnife.bind(this, view);

        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        mAdapter = new TodoListAdapter();
        mAdapter.setFinishListener(item -> mPresenter.updateTodoItem(item));
        mAdapter.setClickedListener((v, item) -> mListener.editTodoItem(item, v.getWidth()/2, (int)v.getY()));
        mAdapter.setDeleteListener(item -> {
            mPresenter.deleteTodoItem(item);
            if (mAdapter.getItemCount() == 0){
                showEmptyView(true);
            }
        });

        recyclerView.setAdapter(mAdapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new SwipeToDeleteCallback(getContext(), mAdapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);

        showEmptyView(true);

        updateTodoList();

        swiperefresh.setOnRefreshListener(() -> mPresenter.loadTodoList());

        return view;
    }

    @Override
    public void updateTodoList(ArrayList<Todo> todoList) {
        swiperefresh.setRefreshing(false);
        mAdapter.setTodoList(todoList);
        mAdapter.notifyDataSetChanged();

        showEmptyView(todoList.size() == 0);
    }

    public void updateTodoList(){
        mPresenter.loadTodoList();
        swiperefresh.setRefreshing(true);
    }

    @Override
    public void showEmptyView(boolean show) {
        gpEmptyList.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public interface TodoListListener {
        void editTodoItem(Todo todo, int x, int y);
    }
}
