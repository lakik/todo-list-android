package com.example.todo_list.ui.main.add;

import com.example.todo_list.model.Todo;

public interface TodoAddContract {
    interface View {
        void closeFragment();
        void showCreateError();
        void showUpdateError();
    }

    interface Presenter {
        void createTodo(Todo todo);
        void updateTodo(Todo todo);
    }
}
