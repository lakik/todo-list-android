package com.example.todo_list.ui.main.add.di;

import com.example.todo_list.ui.main.add.TodoAddFragment;
import com.example.todo_list.ui.main.list.TodoListFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {TodoAddModule.class})
public interface TodoAddComponent {
    void inject(TodoAddFragment fragment);
}
