package com.example.todo_list.ui.main.di;

import com.example.todo_list.ui.main.TodoActivity;
import com.example.todo_list.ui.main.add.di.TodoAddComponent;
import com.example.todo_list.ui.main.add.di.TodoAddModule;
import com.example.todo_list.ui.main.list.di.TodoListComponent;
import com.example.todo_list.ui.main.list.di.TodoListModule;

import dagger.Subcomponent;

@Subcomponent(modules = {TodoMainModule.class})
public interface TodoComponent {
    TodoListComponent add(TodoListModule module);
    TodoAddComponent add(TodoAddModule module);

    void inject(TodoActivity todoActivity);
}

