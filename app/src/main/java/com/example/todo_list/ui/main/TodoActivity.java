package com.example.todo_list.ui.main;

import android.annotation.SuppressLint;
import android.os.Bundle;

import com.example.todo_list.App;
import com.example.todo_list.model.Todo;
import com.example.todo_list.ui.main.add.TodoAddFragment;
import com.example.todo_list.ui.main.di.TodoComponent;
import com.example.todo_list.ui.main.di.TodoMainModule;
import com.example.todo_list.ui.main.list.TodoListFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;

import com.example.todo_list.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TodoActivity extends AppCompatActivity implements TodoContract.View,
                            TodoAddFragment.TodoAddListener,
                            TodoListFragment.TodoListListener{

    public TodoComponent component;

    @Inject TodoPresenter mPresenter;

    @BindView(R.id.fab) FloatingActionButton fab;
    @BindView(R.id.toolbar) Toolbar toolbar;

    TodoListFragment todoListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        component = App.getComponent().add(new TodoMainModule(this));
        component.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        todoListFragment = new TodoListFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, todoListFragment).commit();
    }

    @OnClick(R.id.fab)
    public void fabClicked(View view){
        Bundle bundle = new Bundle();
        bundle.putInt(TodoAddFragment.MOTION_X_ARG, (int)fab.getX() + (int)fab.getPivotX());
        bundle.putInt(TodoAddFragment.MOTION_Y_ARG, (int)fab.getY() - (int)fab.getPivotY());

        setAddFragment(bundle);
    }

    @SuppressLint("RestrictedApi")
    private void setAddFragment(Bundle bundle){
        fab.setVisibility(View.GONE);

        TodoAddFragment fragment = new TodoAddFragment();

        fragment.setArguments(bundle);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.add(R.id.fragment, fragment);
        transaction.addToBackStack(null);

        transaction.commit();
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void showFab() {
        fab.setVisibility(View.VISIBLE);
    }

    @Override
    public void reloadTodoList() {
        todoListFragment.updateTodoList();
    }

    @Override
    public void editTodoItem(Todo todo, int x, int y) {
        Bundle bundle = new Bundle();
        bundle.putInt(TodoAddFragment.MOTION_X_ARG, x);
        bundle.putInt(TodoAddFragment.MOTION_Y_ARG, y);
        bundle.putParcelable(TodoAddFragment.TODO_ITEM, todo);

        setAddFragment(bundle);
    }
}

