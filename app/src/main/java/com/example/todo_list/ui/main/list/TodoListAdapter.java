package com.example.todo_list.ui.main.list;

import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.todo_list.R;
import com.example.todo_list.model.Todo;

import java.util.ArrayList;

public class TodoListAdapter extends RecyclerView.Adapter<TodoListAdapter.ViewHolder>{
    private ArrayList<Todo> todoList = new ArrayList<>();
    private OnTodoFinishedChanged finishListener;
    private OnItemClickedListener clickedListener;
    private OnItemDeleteListener deleteListener;

    Todo deletedTodoItem;
    int deletedTodoItemPosition;

    @Override
    public TodoListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_todo_list_item, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.tvTitle.setText(todoList.get(position).getTitle());
        holder.cbFinished.setChecked(todoList.get(position).isFinished());
        holder.vColor.setBackgroundColor(Color.parseColor(todoList.get(position).getColor()));

        if(todoList.get(position).isFinished()) {
            holder.tvTitle.setPaintFlags( holder.tvTitle.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }else{
            holder.tvTitle.setPaintFlags( holder.tvTitle.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }

        holder.cbFinished.setOnClickListener(v -> {
            Todo todo = todoList.get(position);
            todo.setFinished(((CheckBox)v).isChecked());
            finishListener.onChange(todo);
        });

        holder.itemView.setOnClickListener(v -> {
                Todo todo = todoList.get(position);
                clickedListener.onClick(v, todo);
        });
    }

    public void setTodoList(ArrayList<Todo> todoList) {
        this.todoList = todoList;
    }

    public void setFinishListener(OnTodoFinishedChanged finishListener) {
        this.finishListener = finishListener;
    }

    public void setClickedListener(OnItemClickedListener clickedListener) {
        this.clickedListener = clickedListener;
    }

    public void setDeleteListener(OnItemDeleteListener deleteListener) {
        this.deleteListener = deleteListener;
    }

    @Override
    public int getItemCount() {
        return todoList.size();
    }

    public void deleteItem(int position) {
        deletedTodoItem = todoList.get(position);
        deletedTodoItemPosition = position;
        todoList.remove(position);
        notifyItemRemoved(position);
        deleteListener.onDelete(deletedTodoItem);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;
        private CheckBox cbFinished;
        private View vColor;
        ViewHolder(View v) {
            super(v);

            tvTitle = v.findViewById(R.id.tv_todo_title);
            cbFinished = v.findViewById(R.id.cb_is_finished);
            vColor = v.findViewById(R.id.v_todo_color);
        }
    }

    interface OnTodoFinishedChanged{
        void onChange(Todo item);
    }

    interface OnItemClickedListener{
        void onClick(View v, Todo item);
    }

    interface OnItemDeleteListener{
        void onDelete(Todo item);
    }
}

