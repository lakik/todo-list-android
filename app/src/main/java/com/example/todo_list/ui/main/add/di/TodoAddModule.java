package com.example.todo_list.ui.main.add.di;

import com.example.todo_list.ui.main.add.TodoAddContract;
import com.example.todo_list.ui.main.list.TodoListContract;

import dagger.Module;
import dagger.Provides;

@Module
public class TodoAddModule {

    private TodoAddContract.View mView;

    public TodoAddModule(TodoAddContract.View view) {
        mView = view;
    }

    @Provides
    public TodoAddContract.View provideView() {
        return mView;
    }
}
