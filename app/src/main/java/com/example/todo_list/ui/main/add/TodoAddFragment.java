package com.example.todo_list.ui.main.add;


import android.animation.Animator;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.todo_list.R;
import com.example.todo_list.model.Todo;
import com.example.todo_list.ui.main.TodoActivity;
import com.example.todo_list.ui.main.add.di.TodoAddModule;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import petrov.kristiyan.colorpicker.ColorPicker;


public class TodoAddFragment extends Fragment implements TodoAddContract.View {

    public static String MOTION_X_ARG = "MOTION_X_ARG";
    public static String MOTION_Y_ARG = "MOTION_Y_ARG";
    public static String TODO_ITEM = "TODO_ITEM";

    private TodoAddListener mListener;

    @Inject TodoAddPresenter mPresenter;

    @BindView(R.id.v_color) View vColor;
    @BindView(R.id.et_title) EditText etTitle;
    @BindView(R.id.et_description) EditText etDescription;
    @BindView(R.id.btn_add) Button btnAdd;

    private int selectedColor = -1;

    private Unbinder unbinder;
    private Todo todo;

    public TodoAddFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (getActivity() != null) {
            ((TodoActivity) getActivity()).component.add(new TodoAddModule(this)).inject(this);
            mListener = (TodoActivity)getActivity();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_todo_add, container, false);
        unbinder = ButterKnife.bind(this, view);

        view.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                v.removeOnLayoutChangeListener(this);
                int cx = getArguments().getInt(MOTION_X_ARG);
                int cy = getArguments().getInt(MOTION_Y_ARG);

                float finalRadius = Math.max(v.getWidth(), v.getHeight());
                Animator anim = ViewAnimationUtils.createCircularReveal(v, cx, cy, 0, finalRadius);
                anim.setDuration(250);
                anim.start();
            }
        });

        if (getArguments().getParcelable(TODO_ITEM) != null) {
            todo = getArguments().getParcelable(TODO_ITEM);
            etTitle.setText(todo.getTitle());
            etDescription.setText(todo.getDescription());
            vColor.setBackgroundColor(Color.parseColor(todo.getColor()));

            btnAdd.setText("Update");
        }


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDetach() {
        mListener.showFab();
        super.onDetach();
    }

    @OnClick(R.id.btn_cancel)
    public void btnCancelClicked(){
        getActivity().onBackPressed();
    }

    @OnClick(R.id.v_color)
    public void viewColorClicked() {
        ColorPicker colorPicker = new ColorPicker(getActivity());
        colorPicker.setRoundColorButton(true);
        colorPicker.setDefaultColorButton(selectedColor);
        colorPicker.show();
        colorPicker.setOnChooseColorListener(new ColorPicker.OnChooseColorListener() {
            @Override
            public void onChooseColor(int position,int color) {
                selectedColor = color;
                vColor.setBackgroundColor(color);
            }
            @Override
            public void onCancel(){}
        });
    }

    @OnClick(R.id.btn_add)
    public void btnAddClicked(){

        if (todo == null){
            Todo todo = new Todo();
            setTodo(todo);
            mPresenter.createTodo(todo);
        }else{
            setTodo(todo);
            mPresenter.updateTodo(todo);
        }

    }

    private void setTodo(Todo todo){
        todo.setTitle(etTitle.getText().toString());
        todo.setDescription(etDescription.getText().toString());
        if (selectedColor != -1) {
            todo.setColor("#" + Integer.toHexString(selectedColor).toUpperCase().substring(2));
        }
    }

    @Override
    public void closeFragment() {
        getActivity().onBackPressed();
        mListener.reloadTodoList();
    }

    @Override
    public void showCreateError() {
        Toast.makeText(getContext(), "Error creating", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showUpdateError() {
        Toast.makeText(getContext(), "Error updating", Toast.LENGTH_SHORT).show();
    }

    public interface TodoAddListener{
        void showFab();
        void reloadTodoList();
    }

}


