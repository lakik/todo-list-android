package com.example.todo_list.network;

import com.example.todo_list.R;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


import static com.example.todo_list.App.resources;

/**
 * Created by joaobaptista on 2018-08-30.
 */

public class ApiError {

    // general
    public static final String SERVER_ERROR = resources.getString(R.string.server_error);

    private static final int HTTP_EXPIRED_ERROR = 401;
    public static final int HTTP_PERMISSION_DENIED = 403;
    public static final String INVALID_RESPONSE =  resources.getString(R.string.invalid_response);

    @SerializedName("code")
    @Expose
    private int mHttpCode;


    @SerializedName("error")
    @Expose
    private String mErrorMessage;


    public ApiError(int httpCode, String errorMessage) {
        this(httpCode);
        mErrorMessage = errorMessage;
    }

    public ApiError(int httpCode) {
        mHttpCode = httpCode;
    }

    public String getErrorMessage() {
        if (mErrorMessage == null) {
            mErrorMessage = SERVER_ERROR;
        }

        return mErrorMessage;
    }

    public int getHttpCode() {
        return mHttpCode;
    }
}
