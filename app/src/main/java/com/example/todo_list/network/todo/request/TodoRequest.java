package com.example.todo_list.network.todo.request;

import com.example.todo_list.model.Todo;
import com.google.gson.annotations.SerializedName;

public class TodoRequest {

    @SerializedName("id")
    private String id;

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("color")
    private String color;

    @SerializedName("isFinished")
    private boolean isFinished;


    public TodoRequest(Todo todo) {
        this.id = todo.getId();
        this.title = todo.getTitle();
        this.description = todo.getDescription();
        this.color = todo.getColor();
        this.isFinished = todo.isFinished();
    }
}
