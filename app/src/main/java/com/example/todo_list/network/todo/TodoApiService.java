package com.example.todo_list.network.todo;

import com.example.todo_list.network.todo.request.TodoRequest;
import com.example.todo_list.network.todo.response.TodoDeleteResponse;
import com.example.todo_list.network.todo.response.TodoListResponse;
import com.example.todo_list.network.todo.response.TodoResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface TodoApiService {

    @GET("users/{userID}/todos")
    Call<TodoListResponse> loadTodoList(@Path("userID") String userId);

    @POST("users/{userID}/todos")
    Call<TodoResponse> createTodo(@Path("userID") String userId, @Body TodoRequest todoRequest);

    @PATCH("users/{userID}/todos/{todoID}")
    Call<TodoResponse> updateTodo(@Path("userID") String userId, @Path("todoID") String todoId, @Body TodoRequest todoRequest);

    @DELETE("users/{userID}/todos/{todoID}")
    Call<TodoDeleteResponse> deleteTodo(@Path("userID") String userId, @Path("todoID") String todoId);

}
