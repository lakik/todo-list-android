package com.example.todo_list.network.todo.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TodoDeleteResponse {

    @SerializedName("deleted")
    @Expose
    public boolean mIsDeleted;

    public boolean isDeleted() {
        return mIsDeleted;
    }
}
