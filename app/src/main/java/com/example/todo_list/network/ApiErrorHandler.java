package com.example.todo_list.network;

import android.util.Log;

import com.example.todo_list.R;

import java.lang.annotation.Annotation;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;


import static com.example.todo_list.App.resources;

/**
 * Created by joaobaptista on 2018-08-30.
 */
@Singleton
public class ApiErrorHandler {
    private Retrofit mRetrofit;

    @Inject
    public ApiErrorHandler(Retrofit retrofit) {
        mRetrofit = retrofit;
    }

    public ApiError parseError(Response<?> response) {
        Converter<ResponseBody, ApiError> converter = mRetrofit.responseBodyConverter(ApiError.class, new Annotation[0]);

        ApiError error = new ApiError(0, resources.getString(R.string.server_error));
        try {
            error = converter.convert(response.errorBody());
        } catch (Exception e) {
            Log.e("error", e.getMessage());
        }

        return error;
    }
}
