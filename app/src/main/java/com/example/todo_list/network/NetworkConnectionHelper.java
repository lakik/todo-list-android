package com.example.todo_list.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;

import com.example.todo_list.R;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.example.todo_list.App.resources;

@Singleton
public class NetworkConnectionHelper {

    public static final String NO_INTERNET_CONNECTION = resources.getString(R.string.no_internet_connection);

    private Context mContext;

    @Inject
    public NetworkConnectionHelper(Context context) {
        mContext = context;
    }

    public boolean isAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            NetworkCapabilities capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    return true;
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    return true;
                }  else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)){
                    return true;
                }
            }
        } else {
            try {
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                    Log.i("update_statut", "Network is available : true");
                    return true;
                }
            } catch (Exception e) {
                Log.i("update_statut", "" + e.getMessage());
            }
        }


        return false;
    }


}

