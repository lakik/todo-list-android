package com.example.todo_list.network.todo.response;

import com.example.todo_list.model.Todo;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class TodoResponse {

    @SerializedName("id")
    private String id;

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("lastEditedAt")
    private Date lastEditedTime;

    @SerializedName("color")
    private String color;

    @SerializedName("isFinished")
    private boolean isFinished;

    public long getLastEditedTime() {
        if (lastEditedTime == null) {
            return 0L;
        }

        return lastEditedTime.getTime();
    }

    public Todo build() {
        Todo.Builder builder = new Todo.Builder()
                .setId(id)
                .setTitle(title)
                .setDescription(description)
                .setColor(color)
                .setLastEditedTime(getLastEditedTime())
                .setFinished(isFinished);

        return builder.build();
    }
}
