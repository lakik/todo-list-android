package com.example.todo_list.network.todo.response;

import com.example.todo_list.model.Todo;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TodoListResponse {

    @SerializedName("todos")
    private ArrayList<TodoResponse> todoList;

    public ArrayList<Todo> build() {
        ArrayList<Todo> list = new ArrayList<>();

        for (int i = 0; i < todoList.size(); i++){
            TodoResponse todoResponse = todoList.get(i);

            list.add(todoResponse.build());
        }

        return list;
    }
}
