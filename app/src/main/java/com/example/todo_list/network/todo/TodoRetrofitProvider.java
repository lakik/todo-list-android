package com.example.todo_list.network.todo;

import android.util.Log;

import com.example.todo_list.model.Todo;
import com.example.todo_list.network.ApiError;
import com.example.todo_list.network.ApiErrorHandler;
import com.example.todo_list.network.NetworkConnectionHelper;
import com.example.todo_list.network.todo.request.TodoRequest;
import com.example.todo_list.network.todo.response.TodoDeleteResponse;
import com.example.todo_list.network.todo.response.TodoListResponse;
import com.example.todo_list.network.todo.response.TodoResponse;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TodoRetrofitProvider {

    private TodoApiService mApiService;
    private NetworkConnectionHelper mNetworkConnectionHelper;
    private ApiErrorHandler mErrorHandler;

    @Inject
    public TodoRetrofitProvider(TodoApiService mApiService, NetworkConnectionHelper mNetworkConnectionHelper, ApiErrorHandler mErrorHandler) {
        this.mApiService = mApiService;
        this.mNetworkConnectionHelper = mNetworkConnectionHelper;
        this.mErrorHandler = mErrorHandler;
    }

    public void loadTodoList(String userId, final OnTodoListLoadedListener listener) {
        if (!mNetworkConnectionHelper.isAvailable()) {
            listener.onError(NetworkConnectionHelper.NO_INTERNET_CONNECTION);
            return;
        }

        Call<TodoListResponse> call = mApiService.loadTodoList(userId);
        call.enqueue(new Callback<TodoListResponse>() {
            @Override
            public void onResponse(Call<TodoListResponse> call, Response<TodoListResponse> response) {
                if (response.isSuccessful()) {

                    TodoListResponse todosResponse = response.body();

                    try {
                        listener.onSuccess(todosResponse.build());

                    } catch (Exception e) {
                        Log.e("error", e.getMessage());
                        listener.onError(ApiError.SERVER_ERROR);
                    }
                } else {
                    ApiError error = mErrorHandler.parseError(response);
                    listener.onError(error.getErrorMessage());
                }
            }

            @Override
            public void onFailure(Call<TodoListResponse> call, Throwable t) {
                Log.e("error", t.getMessage());
                listener.onError(ApiError.SERVER_ERROR);
            }
        });
    }

    public void createTodo(String userId, Todo todo, final OnTodoCreatedListener listener) {
        if (!mNetworkConnectionHelper.isAvailable()) {
            listener.onError(NetworkConnectionHelper.NO_INTERNET_CONNECTION);
            return;
        }

        Call<TodoResponse> call = mApiService.createTodo(userId, new TodoRequest(todo));
        call.enqueue(new Callback<TodoResponse>() {
            @Override
            public void onResponse(Call<TodoResponse> call, Response<TodoResponse> response) {
                if (response.isSuccessful()) {

                    TodoResponse todoResponse = response.body();

                    try {
                        listener.onSuccess(todoResponse.build());

                    } catch (Exception e) {
                        Log.e("error", e.getMessage());
                        listener.onError(ApiError.SERVER_ERROR);
                    }
                } else {
                    ApiError error = mErrorHandler.parseError(response);
                    listener.onError(error.getErrorMessage());
                }
            }

            @Override
            public void onFailure(Call<TodoResponse> call, Throwable t) {
                Log.e("error", t.getMessage());
                listener.onError(ApiError.SERVER_ERROR);
            }
        });
    }

    public void updateTodo(String userId, Todo todo, final OnTodoUpdatedListener listener) {
        if (!mNetworkConnectionHelper.isAvailable()) {
            listener.onError(NetworkConnectionHelper.NO_INTERNET_CONNECTION);
            return;
        }

        Call<TodoResponse> call = mApiService.updateTodo(userId, todo.getId(), new TodoRequest(todo));
        call.enqueue(new Callback<TodoResponse>() {
            @Override
            public void onResponse(Call<TodoResponse> call, Response<TodoResponse> response) {
                if (response.isSuccessful()) {

                    TodoResponse todoResponse = response.body();

                    try {
                        listener.onSuccess(todoResponse.build());

                    } catch (Exception e) {
                        Log.e("error", e.getMessage());
                        listener.onError(ApiError.SERVER_ERROR);
                    }
                } else {
                    ApiError error = mErrorHandler.parseError(response);
                    listener.onError(error.getErrorMessage());
                }
            }

            @Override
            public void onFailure(Call<TodoResponse> call, Throwable t) {
                Log.e("error", t.getMessage());
                listener.onError(ApiError.SERVER_ERROR);
            }
        });
    }

    public void deleteTodo(String userId, Todo todo, final OnTodoDeletedListener listener) {
        if (!mNetworkConnectionHelper.isAvailable()) {
            listener.onError(NetworkConnectionHelper.NO_INTERNET_CONNECTION);
            return;
        }

        Call<TodoDeleteResponse> call = mApiService.deleteTodo(userId, todo.getId());
        call.enqueue(new Callback<TodoDeleteResponse>() {
            @Override
            public void onResponse(Call<TodoDeleteResponse> call, Response<TodoDeleteResponse> response) {
                if (response.isSuccessful()) {

                    TodoDeleteResponse todoResponse = response.body();

                    if (todoResponse.isDeleted()){
                        listener.onSuccess();
                    } else {
                        listener.onError(ApiError.SERVER_ERROR);
                    }
                } else {
                    ApiError error = mErrorHandler.parseError(response);
                    listener.onError(error.getErrorMessage());
                }
            }

            @Override
            public void onFailure(Call<TodoDeleteResponse> call, Throwable t) {
                Log.e("error", t.getMessage());
                listener.onError(ApiError.SERVER_ERROR);
            }
        });
    }

    public interface OnTodoListLoadedListener {
        void onSuccess(ArrayList<Todo> todoList);
        void onError(String errorMessage);
    }

    public interface OnTodoCreatedListener {
        void onSuccess(Todo todo);
        void onError(String errorMessage);
    }

    public interface OnTodoUpdatedListener {
        void onSuccess(Todo todo);
        void onError(String errorMessage);
    }

    public interface OnTodoDeletedListener {
        void onSuccess();
        void onError(String errorMessage);
    }
}
