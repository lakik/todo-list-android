package com.example.todo_list;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import com.example.todo_list.di.ApplicationComponent;
import com.example.todo_list.di.ApplicationModule;
import com.example.todo_list.di.DaggerApplicationComponent;
import com.example.todo_list.di.network.NetworkModule;


public class App extends Application  {
    public static Resources resources;

    private static Context appContext;
    private static ApplicationComponent appComponent;

    public static ApplicationComponent getComponent() {
        return appComponent;
    }

    public static Context getAppContext() {
        return appContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        resources = getResources();
        appContext = getApplicationContext();

        appComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule((App.this)))
                .build();

    }


}
