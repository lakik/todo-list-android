package com.example.todo_list.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Todo implements Parcelable {
    private String id;
    private String title;
    private String description;
    private long lastEditedTime;
    private String color;
    private boolean isFinished;

    public Todo() {
    }

    public Todo(String id, String title, String description, long lastEditedTime, String color, boolean isFinished) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.lastEditedTime = lastEditedTime;
        this.color = color;
        this.isFinished = isFinished;
    }

    public String getTitle() {
        return title;
    }

    public long getLastEditedTime() {
        return lastEditedTime;
    }

    public String getColor() {
        return color;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLastEditedTime(long lastEditedTime) {
        this.lastEditedTime = lastEditedTime;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setFinished(boolean finished) {
        isFinished = finished;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    public static class Builder{
        private String id;
        private String title;
        private String description;
        private long lastEditedTime;
        private String color;
        private boolean isFinished;

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setLastEditedTime(long lastEditedTime) {
            this.lastEditedTime = lastEditedTime;
            return this;
        }

        public Builder setColor(String color) {
            this.color = color;
            return this;
        }

        public Builder setFinished(boolean finished) {
            isFinished = finished;
            return this;
        }

        public Builder setId(String id) {
            this.id = id;
            return this;
        }

        public Builder setDescription(String description) {
            this.description = description;
            return this;
        }

        public Todo build(){
            return new Todo(id, title, description, lastEditedTime, color, isFinished);
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeLong(this.lastEditedTime);
        dest.writeString(this.color);
        dest.writeByte(this.isFinished ? (byte) 1 : (byte) 0);
    }

    protected Todo(Parcel in) {
        this.id = in.readString();
        this.title = in.readString();
        this.description = in.readString();
        this.lastEditedTime = in.readLong();
        this.color = in.readString();
        this.isFinished = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Todo> CREATOR = new Parcelable.Creator<Todo>() {
        @Override
        public Todo createFromParcel(Parcel source) {
            return new Todo(source);
        }

        @Override
        public Todo[] newArray(int size) {
            return new Todo[size];
        }
    };
}
