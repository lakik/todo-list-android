package com.example.todo_list.data.todo;

import com.example.todo_list.model.Todo;
import com.example.todo_list.network.todo.TodoRetrofitProvider;

import javax.inject.Inject;

public class TodoRemoteDatasource {

    private final TodoRetrofitProvider todoRetrofitProvider;

    @Inject
    public TodoRemoteDatasource(TodoRetrofitProvider todoRetrofitProvider) {
        this.todoRetrofitProvider = todoRetrofitProvider;
    }

    public void loadTodoList(String userId, final TodoRetrofitProvider.OnTodoListLoadedListener listener) {
        todoRetrofitProvider.loadTodoList(userId, listener);
    }

    public void createTodo(String userId, Todo todo, final TodoRetrofitProvider.OnTodoCreatedListener listener) {
        todoRetrofitProvider.createTodo(userId, todo, listener);
    }

    public void updateTodo(String userId, Todo todo, final TodoRetrofitProvider.OnTodoUpdatedListener listener) {
        todoRetrofitProvider.updateTodo(userId, todo, listener);
    }

    public void deleteTodo(String userId, Todo todo, final TodoRetrofitProvider.OnTodoDeletedListener listener) {
        todoRetrofitProvider.deleteTodo(userId, todo, listener);
    }

}
