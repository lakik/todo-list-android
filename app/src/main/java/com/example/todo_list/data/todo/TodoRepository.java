package com.example.todo_list.data.todo;

import com.example.todo_list.data.user.UserRepository;
import com.example.todo_list.model.Todo;
import com.example.todo_list.network.todo.TodoRetrofitProvider;

import java.util.ArrayList;

import javax.inject.Inject;

public class TodoRepository {

    private final TodoLocalDataSource todoLocalDataSource;
    private final TodoRemoteDatasource todoRemoteDatasource;
    private final UserRepository userRepository;

    @Inject
    public TodoRepository(TodoLocalDataSource todoLocalDataSource, TodoRemoteDatasource todoRemoteDatasource, UserRepository userRepository) {
        this.todoLocalDataSource = todoLocalDataSource;
        this.todoRemoteDatasource = todoRemoteDatasource;
        this.userRepository = userRepository;
    }

    public void loadTodoList(final OnTodoListListener listener){
        todoRemoteDatasource.loadTodoList(userRepository.getUserId(), new TodoRetrofitProvider.OnTodoListLoadedListener() {
            @Override
            public void onSuccess(ArrayList<Todo> todoList) {
                todoLocalDataSource.setTodoList(todoList);
                listener.OnLoaded(todoList);
            }

            @Override
            public void onError(String errorMessage) {
                listener.OnError(errorMessage);
            }
        });
    }

    public void createTodo(Todo todo, final OnCreateTodoListener listener) {
        todoRemoteDatasource.createTodo(userRepository.getUserId(), todo, new TodoRetrofitProvider.OnTodoCreatedListener() {
            @Override
            public void onSuccess(Todo todo) {
                listener.OnCreated(todo);
            }

            @Override
            public void onError(String errorMessage) {
                listener.OnError(errorMessage);
            }
        });
    }

    public void updateTodo(Todo todo, final OnUpdateTodoListener listener) {
        todoRemoteDatasource.updateTodo(userRepository.getUserId(), todo, new TodoRetrofitProvider.OnTodoUpdatedListener() {
            @Override
            public void onSuccess(Todo todo) {
                listener.OnUpdated(todo);
            }

            @Override
            public void onError(String errorMessage) {
                listener.OnError(errorMessage);
            }
        });
    }

    public void deleteTodo(Todo todo, final OnDeleteTodoListener listener) {
        todoRemoteDatasource.deleteTodo(userRepository.getUserId(), todo, new TodoRetrofitProvider.OnTodoDeletedListener() {
            @Override
            public void onSuccess() {
                listener.OnDeleted();
            }

            @Override
            public void onError(String errorMessage) {
                listener.OnError(errorMessage);
            }
        });
    }



    public interface OnTodoListListener {
        void OnLoaded(ArrayList<Todo> todoList);
        void OnError(String errorMessage);
    }

    public interface OnCreateTodoListener {
        void OnCreated(Todo todo);
        void OnError(String errorMessage);
    }

    public interface OnUpdateTodoListener {
        void OnUpdated(Todo todo);
        void OnError(String errorMessage);
    }

    public interface OnDeleteTodoListener {
        void OnDeleted();
        void OnError(String errorMessage);
    }
}

