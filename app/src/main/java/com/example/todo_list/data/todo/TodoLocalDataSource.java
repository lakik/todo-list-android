package com.example.todo_list.data.todo;

import com.example.todo_list.model.Todo;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class TodoLocalDataSource {

    private ArrayList<Todo> todoList = new ArrayList<>();

    @Inject
    public TodoLocalDataSource() {
    }

    public ArrayList<Todo> getTodoList() {
        return todoList;
    }

    public void setTodoList(ArrayList<Todo> todoList) {
        this.todoList = todoList;
    }
}
