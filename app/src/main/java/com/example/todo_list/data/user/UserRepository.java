package com.example.todo_list.data.user;


import android.content.Context;
import android.content.SharedPreferences;

import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class UserRepository {
    private static final String KEY = UserRepository.class.getSimpleName();
    private static final String PREF_USER_ID = "user_id";


    private SharedPreferences mPreferences;
    private String userId = "";

    @Inject
    public UserRepository(Context context) {
        mPreferences = context.getSharedPreferences(KEY, Context.MODE_PRIVATE);

        userId = loadUserId();
    }

    public String getUserId() {
        if (userId.isEmpty()){
            userId = UUID.randomUUID().toString();

            saveUserId(userId);
        }

        return userId;
    }

    private String loadUserId(){
        return mPreferences.getString(PREF_USER_ID, "");
    }

    private void saveUserId(String userId){
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(PREF_USER_ID, userId);
        editor.apply();
    }
}
